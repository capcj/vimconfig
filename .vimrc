filetype indent on
set tabstop=4 shiftwidth=4 expandtab
syntax on

" Speed up scrolling
set ttyfast

" Status bar
set laststatus=2

" Display options
set showmode showcmd

" Show line numbers
set number

" Encoding
set encoding=utf-8

" Highlight search matching patterns
set hlsearch

